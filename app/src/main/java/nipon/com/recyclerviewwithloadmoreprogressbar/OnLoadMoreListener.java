package nipon.com.recyclerviewwithloadmoreprogressbar;

public interface OnLoadMoreListener {
    void onLoadMore();
}
